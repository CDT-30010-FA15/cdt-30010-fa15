Reading 05 - README
===================

Readings
--------

The readings for **Tuesday, September 29** are:

1. [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/)

    - [Chapter 4. Lists](https://automatetheboringstuff.com/chapter4)
    - [Chapter 6. Manipulating Strings](https://automatetheboringstuff.com/chapter6)

2. [Python for Informatics: Exploring Information](http://www.pythonlearn.com/)

    - [Chapter 6. Strings](http://www.pythonlearn.com/html-270/book007.html)
    - [Chapter 8. Lists](http://www.pythonlearn.com/html-270/book009.html)

[Python]: http://python.org
[Online Python Tutor]: http://www.pythontutor.com/
[comma-separated-value]: https://en.wikipedia.org/wiki/Comma-separated_values

Questions
---------

Once you have completed these readings, please answer the following questions:

1. What is the difference between a **list** and a **string**?  What can you do
   with a **list** that you can't do with a **string** and vice versa?

2. Write a [Python] function, **wsv_to_csv**, that given a string **s** in
   whitespace separated format, the function returns a string in
   [comma-separated-value] format:

	    :::python
	    def wsv_to_csv(s):
	        ''' Return comma-separated-value string '''
	        # TODO: Implement function

	    >>> wsv_to_csv('Shake down the thunder!')
	    Shake,down,the,thunder!

Feedback
--------

If you have any questions, comments, or concerns regarding the course, please
provide your feedback here.
