Reading 00 - README
===================

Readings
--------

The readings for **Tuesday, August 24th** are:

1. [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/)

    - [Chapter 0. Introduction](https://automatetheboringstuff.com/chapter0)

2. [Python for Informatics: Exploring Information](http://www.pythonlearn.com/)

    - [Chapter 1. Why should you learn to write programs?](http://www.pythonlearn.com/html-270/book002.html)

3. [Teach Yourself Programming in Ten Years](http://norvig.com/21-days.html)

Questions
---------

There are no questions for this reading.

Feedback
--------

If you have any questions, comments, or concerns regarding the course, please
provide your feedback here.
