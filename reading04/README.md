Reading 04 - README
===================

Readings
--------

The readings for **Tuesday, September 22** are:

1. [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/)

    - [Chapter 3. Functions](https://automatetheboringstuff.com/chapter3)

2. [Python for Informatics: Exploring Information](http://www.pythonlearn.com/)

    - [Chapter 4. Functions](http://www.pythonlearn.com/html-270/book005.html)

[Python]: http://python.org
[code]: http://www.pythontutor.com/visualize.html#code=x+%3D+0%0A%0Adef+increment(x%29%3A%0A++++x+%3D+x+%2B+1%0A++++return+x%0A%0Aprint+x%0Aprint+increment(x%29%0Aprint+x%0Aprint+increment(x%29&mode=display&origin=opt-frontend.js&cumulative=false&heapPrimitives=false&textReferences=false&py=2&rawInputLstJSON=%5B%5D&curInstr=14
[Online Python Tutor]: http://www.pythontutor.com/

Questions
---------

Once you have completed these readings, please answer the following questions:

1. What is the difference between using a **return** statement and using a
   **print** statement at the end of a function?

2. What is the result of the following [Python] code:

	    :::python
	    x = 0
	    def increment(x):
	        x = x + 1
	        return x

	    print x
	    print increment(x)
	    print x
	    print increment(x)

    Explain what is happening at each **print** statement.

    **Hint:** Trace through the [code] using the [Online Python Tutor].

Feedback
--------

If you have any questions, comments, or concerns regarding the course, please
provide your feedback here.
