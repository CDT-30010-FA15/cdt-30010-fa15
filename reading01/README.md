Reading 01 - README
===================

Readings
--------

The readings for **Tuesday, August 31** are:

1. [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/)

    - [Chapter 1. Python Basics](https://automatetheboringstuff.com/chapter1)

2. [Python for Informatics: Exploring Information](http://www.pythonlearn.com/)

    - [Chapter 2. Variables, expressions, and statements](http://www.pythonlearn.com/html-270/book003.html)

[Python]: http://python.org

Questions
---------

Once you have completed these readings, please answer the following questions:

1. In your own words, explain the difference between an **expression** and a
   **statement** in [Python] and provide an example of each.

2. Given the following [Python] code, identify any errors, explain what is
   wrong, and then fix the problems.

	    :::python
	    # Print the number of Notre Dame Heisman winners
	    Heisman Winners = 7
	    print 'Notre Dame has ' + Heisman Winners + ' Heisman trophy winners'

Feedback
--------

If you have any questions, comments, or concerns regarding the course, please
provide your feedback here.
