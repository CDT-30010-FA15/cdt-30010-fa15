Reading 02 - README
===================

Readings
--------

The readings for **Tuesday, September 08** are:

1. [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/)

    - [Chapter 2. Flow Control](https://automatetheboringstuff.com/chapter2)
	
	*Read until you reach while Loop Statements*.

2. [Python for Informatics: Exploring Information](http://www.pythonlearn.com/)

    - [Chapter 3. Conditional Execution](http://www.pythonlearn.com/html-270/book004.html)

[Python]: http://python.org

Questions
---------

Once you have completed these readings, please answer the following questions:

1. Simplify the following [Python] code into a single line of code:

        :::python
        if n == 0:
            b = True
        else:
            b = False

    **Hint:** What is the result of **n == 0**?

2. Write [Python] code that prints **'Yeah'** if a number **n** is between the
   values **a** and **b** (inclusive). Otherwise, print **'Nope'**.

Feedback
--------

If you have any questions, comments, or concerns regarding the course, please
provide your feedback here.
