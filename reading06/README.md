Reading 06 - README
===================

Readings
--------

The readings for **Tuesday, October 06** are:

1. [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/)

    - [Chapter 5. Dictionaries and Structuring Data](https://automatetheboringstuff.com/chapter5)

2. [Python for Informatics: Exploring Information](http://www.pythonlearn.com/)

    - [Chapter 9. Dictionaries](http://www.pythonlearn.com/html-270/book010.html)

[Python]: http://python.org
[code]: http://www.pythontutor.com/visualize.html#code=x+%3D+0%0A%0Adef+increment(x%29%3A%0A++++x+%3D+x+%2B+1%0A++++return+x%0A%0Aprint+x%0Aprint+increment(x%29%0Aprint+x%0Aprint+increment(x%29&mode=display&origin=opt-frontend.js&cumulative=false&heapPrimitives=false&textReferences=false&py=2&rawInputLstJSON=%5B%5D&curInstr=14
[Online Python Tutor]: http://www.pythontutor.com/

Questions
---------

Once you have completed these readings, please answer the following questions:

1. What is the difference between a **list** and a **dict**? When would we
   prefer one over the other?

2. Write a [Python] function, **digits_to_text**, that converts any digits (ie.
   **0 - 9**) in a string to the corresponding English word:

        :::python
        def digits_to_text(text):
            ''' Converts any digits in text to words '''
            # TODO: Implement function

        >>> digits_to_text('A B C 1 2 3')
        A B C one two three

Feedback
--------

If you have any questions, comments, or concerns regarding the course, please
provide your feedback here.
