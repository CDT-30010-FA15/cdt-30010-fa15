Reading 03 - README
===================

Readings
--------

The readings for **Tuesday, September 15** are:

1. [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/)

    - [Chapter 2. Flow Control](https://automatetheboringstuff.com/chapter2)

	*Read the rest of the chapter*

2. [Python for Informatics: Exploring Information](http://www.pythonlearn.com/)

    - [Chapter 5. Iteration](http://www.pythonlearn.com/html-270/book006.html)

[Python]: http://python.org
[code]: http://pythontutor.com/visualize.html#code=n+%3D+0%0Atotal+%3D+0%0Awhile+n+%3C+10%3A%0A++++total+%2B+n%0Aprint+total&mode=display&origin=opt-frontend.js&cumulative=false&heapPrimitives=false&textReferences=false&py=2&rawInputLstJSON=%5B%5D&curInstr=0
[Online Python Tutor]: http://www.pythontutor.com/

Questions
---------

Once you have completed these readings, please answer the following questions:

1. What is the difference between a **while** loop and a **for** loop?  When
   would you use one over the other?

2. Given the following [Python] code, identify any errors, explain what is
   wrong, and then fix the problems.

        :::python
        # Print the sum of the integers 0 through 9 using a while loop
        n = 0
        total = 0
        while n < 10:
            total + n
        print total

    **Hint:** Trace through the [code] using the [Online Python Tutor].

Feedback
--------

If you have any questions, comments, or concerns regarding the course, please
provide your feedback here.
